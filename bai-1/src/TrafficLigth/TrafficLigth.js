import React, { Component } from 'react';
import './TrafficLigth.css';
import ClassNames from 'classnames'

const RED = 0;
const GREEN = 1;
const YELLOW = 2;

class TrafficLigth extends Component {
	render() {
		const { currenColor } = this.props;
		console.log(currenColor)
		return <div className='TrafficLigth'>
		<div className={ClassNames('build', 'red', {
			active: currenColor === RED	
		})}/>
		<div className={ClassNames('build', 'green', {
			active: currenColor === GREEN	
		})}/>
		<div className={ClassNames('build', 'yellow', {
			active: currenColor === YELLOW	
		})}/>
		</div>
	}

}
export default TrafficLigth;