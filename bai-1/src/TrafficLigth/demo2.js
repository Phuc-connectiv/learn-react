import React, { Component } from 'react';
import TrafficLigth from './TrafficLigth';

const RED = 0;
const GREEN = 1;
const YELLOW = 2; 

class Demo2 extends Component {
	constructor() {
		super();
		this.state = {
			 currenColor: RED
		};
		setInterval(()=> {
			console.log(this.state.currenColor);
			this.setState({
				currenColor: this.getNextColor(this.state.currenColor)
			});
		},1000);
	}
	getNextColor(color) {
		switch(color){
			case RED: return GREEN;
			case GREEN: return YELLOW;
			default: return RED;
		}
	}
	render(){
		const { currenColor } = this.state
		return (
			<TrafficLigth currenColor = {currenColor}/>
		);
	}
}
export default Demo2;