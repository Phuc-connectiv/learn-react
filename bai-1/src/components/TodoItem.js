import React, { Component } from 'react';
import classNames from 'classnames'
import './todo-item.css';

class TodoItem extends Component {
	render() {
	const { i } = this.props;
		return (
		<div onClick={this.onItemClick} className={classNames('todo-item',{
			'todo-item-done': i.done
		})}>
		<p>{this.props.i.title}</p>
		</div>
		);
	}
}
export default TodoItem;