import React, { Component } from 'react';
import TodoItem from './TodoItem';

class Demo extends Component {
	constructor() {
    super();
    this.todoItem = [
    {title:'oto', done: true},
    {title:'moto'},
    {title:'ipad'}
    ];
  }
	render() {
		return (
			this.todoItem.length > 0 ? this.todoItem.map((i, index) => 
				<TodoItem key={index} i={i} />) : 'Nothing in your eyes'
		);
	}
}
	export default Demo;