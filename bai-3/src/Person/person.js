import React from 'react';
import './person.css'

const person = (props) => {
	return(
		<div className='person'>
			<p onClick={props.click} >Iam {props.name} and i am {Math.floor(Math.random() * 30)} year old</p>
			<p>({props.children})</p>
			<input type='text' onChange={props.changed} value={props.name}/>
		</div>
	);
};
export default person;