import React,{Component} from 'react';
/*import logo from './logo.svg';*/
import Person from './Person/person'
import './App.css';

/*const App = props => {
	const [ personState, setPersonState ] = useState({
		persons: [
		{ name: 'Phuc', age: 21 },
		{ name: 'Cong', age: 25 },
		{ name: 'Tuyen', age: 29 }
		]
	});
	const switchName = () => {
		setPersonState({
		persons: [
		{ name: 'Huyen', age: 21 },
		{ name: 'Cong', age: 25 },
		{ name: 'Tuyen', age: 29 }
		]
			})
	}
  	return (
  		<div className='App'>
  			<button onClick={switchName}>Switch Name</button>
  			<Person 
  			name={personState.persons[0].name} 
  			age={personState.persons[0].age}
  			/>
  			<Person 
  			name={personState.persons[1].name} 
  			age={personState.persons[1].age}
  			/>
  			<Person 
  			name={personState.persons[2].name} 
  			age={personState.persons[2].age}
  			/>
  		</div>
  	);
}*/
class App extends Component{
	state = {
		persons: [
		{ name: 'Phuc', age: 21 },
		{ name: 'Cong', age: 25 },
		{ name: 'Tuyen', age: 29 }
		]
	}
	nameChangeHandler = (event) => {
		this.setState({
		persons: [
		{ name: event.target.value, age: 21 },
		{ name: 'Cong', age: 25 },
		{ name: 'Tuyen', age: 29 }
		]
		})
	}
	switchName = (name) => {
		this.setState({
		persons: [
		{ name: 'Phuc', age: 21 },
		{ name: 'Cong', age: 25 },
		{ name: 'Tuyen', age: 29 }
		]
			})
	}
  render(){
  	//return React.createElement('div', {className: 'App'}, React.createElement('h1',null,'hi!'));
  	const style = {
  		backgroundColor: 'white',
  		font: 'inherit',
  		border: '1px solid blue',
  		padding: '8px',
  		cursor: 'pointer'
  	};
  	return (
  		<div className='App'>
  			<button 
  			style={style}
  			onClick={() => this.switchName('lala') }>
  			Switch Name
  			</button>
  			<Person 
  			click={this.switchName.bind(this,'kkk')} 
  			changed={this.nameChangeHandler}
  			name={this.state.persons[0].name} 
  			age={this.state.persons[0].age}/>
  			<Person 
  			name={this.state.persons[1].name} 
  			age={this.state.persons[1].age}/>
  			<Person 
  			name={this.state.persons[2].name} 
  			age={this.state.persons[2].age}>hehe</Person>
  		</div>
  	);
  }
}

export default App;
