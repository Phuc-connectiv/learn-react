import React, {Component} from 'react';
import ClassNames from 'classnames'

import './list-todo.css';

class ListTodo extends Component {
	render() {
		const {item} = this.props;
		return (
			<div className={ClassNames('list-todo', {
				'list-todo-status': item.status
			})}>
				{this.props.item.play}
			</div>
		);
	}
}
export default ListTodo;